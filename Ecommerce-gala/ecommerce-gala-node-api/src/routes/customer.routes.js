const customerController = require("../controller/customer.controller")
const customer = (app)=>{
    app.get("/api/customer/getList",customerController.getList)
    app.post("/api/customer/create",customerController.create)
    app.post("/api/customer/login",customerController.login)
    app.put("/api/customer/update",customerController.update)
    app.delete("/api/customer/delete/:id",customerController.remove)
    app.get("/api/customer/getCart",customerController.getCart)
    
}
module.exports=customer;