
const customerController = require("../controller/user.controller")
const user = (app) =>{
    app.get("/api/user/getList",customerController.getList)
    app.get("/api/user/create",customerController.create)
    app.get("/api/user/update",customerController.update)
    app.get("/api/user/delete",customerController.remove)
}

module.exports = user