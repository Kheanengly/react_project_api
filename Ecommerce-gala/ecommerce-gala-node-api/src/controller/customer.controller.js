 const db = require("../config/db.config");
const { Config } = require("../util/service");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const secret_access_token = "EITLDS@$*&^KJHL5973PXNDMB84753";
var dataCarts = [
   {
      id:101,
      name:"Jean",
      color:"red",
      customer:1
   },
   {
      id:102,
      name:"Men Jean",
      color:"red",
      customer:2
   },
   {
      id:103,
      name:"Women Jean",
      color:"red",
      customer:3
   },
   {
      id:104,
      name:"Engfong",
      color:"red",
      customer:56
   }
]
const getList = (req,res)=>{
   var query = req.query;
   var text_search = query.text_search;
   var page =query.page;
   
   console.log(req.query)
   console.log(req.query.page) ;
   console.log(req.query.text_search);
   var sqlSelect = "SELECT * from customer "
   if(text_search != null){
      sqlSelect += "WHERE firstname LIKE '%"+text_search+"%' ";
   }
   sqlSelect += "order by firstname ASC ";
   var offset = (page-1)*Config.pagination;
   sqlSelect += "LIMIT "+offset+","+Config.pagination+";";
   // call query function  to input sql statement to select data from database 
   console.log(sqlSelect);
    db.query(sqlSelect,(err,rows)=>{
         db.query("select count(customer_id) as total From customer",(e1,row1) => {
             // and then conver respont data to json data 
            var total_recode = row1[0].total;
            console.log(row1);
            res.json({
               // add key total to data
               total_recode : total_recode,
               pagenation:Config.pagination,
               // total_page:Math.ceil(total_recode/Config.pagination),
               list_customers:rows
            })
         })
        
    })
 }

 const getCart = (req,res) => {
   var authorization = req.headers.authorization;
   var token_from_client = null;
   if(authorization != null && authorization != ""){
      token_from_client = authorization.split(" ")
      token_from_client = token_from_client[1];

   }
   if(token_from_client == null){
      res.json({
         error:true,
         message:"You have permission access this method"
      })
   }else{
      jwt.verify(token_from_client,secret_access_token,(err,data)=>{
         if(err){
            res.json({
               error:true,
               message:"Invalid token!", 
            })
         }else{
            var customer_id = data.profile.customer_id;
            var cart = dataCarts.filter((item,index)=>item.customer == customer_id);
            res.json({ 
               cart:cart,
            })
         }
      })
   }

  
 }

 const create = (req,res)=>{

   // test encrypt password and respone
   // var password = "123"
   // var bcrypt_password = bcrypt.hashSync(password,10)
   // var paraPassword = "12"
   // var isCorrect = bcrypt.compareSync(paraPassword,bcrypt_password)
   // res.json({
   //    password : password,
   //    bcrypt_password : bcrypt_password,
   //    isCorrect : isCorrect
   // })
   // return 

   // let varaible to require data from api when we post data from api or link url by json data (object data) from postman or thunder tool and user input on fronend
   var body = req.body;
   if(body.firstname == null || body.firstname == ""){
      res.json({
          error:true,
          message : "Please fill in firstname"
      })
      return false
  }
  if(body.lastname == null ||  body.lastname == ""){
      res.json({
          error:true,
          message : "Please fill in lastname"
      })
      return false
  }

   if(body.username == null || body.username == ""){
      res.json({
         error : true,
         message : "Please fill in username",
      })
      return false
   }else{
      // username is email or tel
      // 
   }

   if(body.password == null || body.password == ""){
      res.json({
         error : true,
         message : "Please fill in password",
      })
      return false
   }

   db.query("select * from customer where email = ?",[username],(err,rows)=>{
      if(err){
         res.json({
            error : true,
            message: err,
         })
      }else{
         if(rows.length==0){
            var password = bcrypt.hashSync(body.password,10)
            var sqlInsert = "INSERT INTO customer ( firstname, lastname, gender, dob, email,password, is_active) VALUES (?,?,?,?,?,?,?)" ; 
             db.query(sqlInsert,[body.firstname, body.lastname, body.gender, body.dob,body.username,password, body.is_active],(error,rows) => { // declare value for sql statement with haddile parameter and statement
               // check error if have and display error by assigt error : true and display message of error
               if(error){
                  res.json({
                     error : true,
                     message : error,
                  })
               }else{
                  // respont data ដែលត្រូវបោះទៅ database
                  res.json({
                     message : "Customer inserted!",
                     data:rows,
                  })
               }
            })

         }else{
            res.json(
               {
                  err:true,
                  message: "Account already exist!"
               } 
            )
         }
      }
   })
  
 }

 const login = (req,res) => {
   var {username,password} = req.body;
   if(username == null || username == ""){
      res.json({
         error : true,
         message : "Please fill in username!",
      })
      return
   }else if(password == null || password == ""){
      res.json({
         error : true,
         message : "Please fill in password!",
      })
      return
   }

   db.query("SELECT * FROM customer WHERE email = ?",[username],(err,result)=>{
      if(err){
         res.json({
            error: true,
            message : err
         })
      }else{
         if(result.length == 0){
            res.json({
               error:true,
               message:"User dose not exit. Please register!"
            })
         }else{
            var data = result[0]
            var passwordInDB = data.password;
            var isCorrectPassword = bcrypt.compareSync(password,passwordInDB) // true/false
            if(isCorrectPassword){
               delete data.password;
               var token = jwt.sign({profile:data},secret_access_token);
               res.json({
                  message : "Login Success",
                  profile : data,
                  token:token
               })
            }else{
               res.json({
                  message : "Incorrect password!"
               })
            }
         }
      }
   })
 }

 const update = (req,res)=>{
// let varaible to require data from api when we post data from api or link url by json data (object data) from postman or thunder tool and user input on fronend
   var body = req.body;
   if(body.firstname == null || body.firstname == ""){
      res.json({
          error:true,
          message : "Please fill in firstname"
      })
      return false
  }
  if(body.lastname == null ||  body.lastname == ""){
      res.json({
          error:true,
          message : "Please fill in lastname"
      })
      return false
  }
  var sqlUpdate = "UPDATE customer SET firstname=?, lastname=?, gender=?, dob=?, tel=?, email=?, is_active=? WHERE customer_id = ?"
  db.query(sqlUpdate,[body.firstname, body.lastname, body.gender, body.dob, body.tel, body.email, body.is_active,body.customer_id],(error,rows) => { // declare value for sql statement with haddile parameter and statement
      // check error if have and display error by assigt error : true and display message of error
      if(error){
         res.json({
            error : true,
            message : error
         })
      }else{
         // respont data ដែលត្រូវបោះទៅ database
         res.json({
            message : "Customer inserted!",
            data : rows
         }) 
      }
   })
 }

 const remove = (req,res) => {
   console.log(req.params.id)
   // decalre one variable to assign statement
   var sqlInsert = "delete from customer where customer_id = "+req.params.id;
   db.query(sqlInsert,(error,rows) => {
      // erroe is message by error machine
      if(error){
         res.json({
            error : true,
            message : error,
         })
      }else{
         if(rows.affectedRows !=0){ // affectedRow not eqaul 0 data delete from database already
            res.json({
               message : "Customer deleted!",
               data:rows,
            })
         }else{
            res.json({
               message : "Delete Not complete. Customer not found",
               data:rows,
            })
         }
      
      }
   })
 }


 module.exports ={
    getList,
    create,
    update,
    remove,
    login,
    getCart
 }