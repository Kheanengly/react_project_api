
:: Node
- init node 
    > npm init  create json pakage
- create file index.js
    and then add 
    var x =10;
    console.log("X = "+x); 
- run this file (index.js)
    > node index.js
- install librairy (express) is optimai config route in project
    > npm install express
- install nodemon for recompile auto
    > npm install nodemon
- install libaray mysql 
    > npm install mysql 
- install libaray cors is to allow another user to reqire data 
    > npm install cors

:: React 
- check verssion node and npm 
    > node -v
    > npm -v
- create new project react 
    > npx create-react-app name_project
- run code in react 
    > npm start 
- install axios to fecth data from url 
    > npm install axios
- install ant-desing libray by use
    > npm install antd
- install icon of ant 
    > npm i @ant-design/icons
- install moment for format date
    > npm i moment 