const express = require("express")
const app = express()
const cors = require("cors");
app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use(cors({ // allow all resoure to another user require data
    origin:"*"
}))
app.get("/",(req,res)=>{
    res.send("Hello Node.js");
})
app.get("/Engly_store/product",(req,res)=>{
    var data=[
        {
            id:10,
            name:"Coca",
            qty:10
        },
        {
            id:11,
            name:"juice",
            qty:10
        }
    ]
    res.send(data);
})



// require from folder router , controller and config
require("./src/routes/customer.routes")(app);
require("./src/routes/user.route")(app)
// way root to port server
const port = 8080;
app.listen(port,()=>{
    console.log("http://localhost: "+ port)
    // cosole.log(`http://localhost: ${port}`)
})
